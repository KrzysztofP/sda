import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class PierwszySwing implements ActionListener {

	JTextField jtf;
	JTextField MojePole;
	JButton jbtnRev;
	JLabel jlabPrompt;
	JLabel mojaramka, mojaramka2;
	JLabel Result;

	PierwszySwing() {
		JFrame jfrm = new JFrame("Pole tekstowe");
		jfrm.setLayout(new FlowLayout());
		jfrm.setSize(340, 140);
		jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		jtf = new JTextField(10);
		jtf.setActionCommand("MyTF");

		MojePole = new JTextField(10);
		MojePole.setActionCommand("WTF????");

		JButton jbtnRev = new JButton("Oblicz");

		jtf.addActionListener(this);
		MojePole.addActionListener(this);
		jbtnRev.addActionListener(this);

		jlabPrompt = new JLabel("Wprowadz odleglosc");
		Result = new JLabel("");

		mojaramka = new JLabel("Wprowadz promien kola");
		mojaramka2 = new JLabel("");

		jfrm.add(jlabPrompt);
		jfrm.add(jtf);
		jfrm.add(MojePole);
		jfrm.add(mojaramka);
		jfrm.add(mojaramka2);
		jfrm.add(jbtnRev);
		jfrm.add(Result);
		jfrm.setVisible(true);
	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("Oblicz")) {
			double orgStr =  Integer.parseInt(jtf.getText());
			double odleglosc = Integer.parseInt(MojePole.getText());
			int wyniczek = (int) (3.141592654*Math.pow(orgStr, 2)-0.25*3.141592654*Math.pow(odleglosc, 2));
			String resStr = Integer.toString(wyniczek);
			//3.141592654*Math.pow(r, 2)-0.25*3.141592654*Math.pow(d, 2);
			//for (int i = orgStr.length() - 1; i >= 0; i--)
				//resStr += orgStr.charAt(i);

			Result.setText("Wynik to: " + resStr);
		} else
			Result.setText(".... " + jtf.getText());
	}
	
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new PierwszySwing();
			}
		});
	}
}
