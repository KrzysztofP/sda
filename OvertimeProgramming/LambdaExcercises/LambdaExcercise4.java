interface StringTest{
	boolean test(String aStr, String bStr);
}

public class LambdaExcercise4 {
	public static void main(String[] args) {
		StringTest isIn = (a, b) -> a.indexOf(b) != -1;
		String str = "To jest test";
		
		System.out.println("Sprawdzam lancuch znakowy: " + str);
		
		if(isIn.test(str, "jest"))
			System.out.println("Znaleziono lancuch 'jest'");
		else
			System.out.println("Nie znaleziono lancucha 'jest'");
		
		if(isIn.test(str, "xyz"))
			System.out.println("Znaleziono lancuch 'xyz'");
		else
			System.out.println("Nie znaleziono lancucha 'xyz'");
	}

}
