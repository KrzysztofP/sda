interface NumericTest{
	boolean test(int n, int m);
}

public class LambdaExcercise1 {
	public static void main(String[] args) {
		NumericTest isFactor = (n, d) -> (n % d) == 0;
		if(isFactor.test(10, 2))
			System.out.println("Liczba 2 jest czynnikiem liczby 10");
		if(isFactor.test(10,3))
			System.out.println("Liczba 3 nie jest czynnikiem liczby 10");
		System.out.println();
		
		NumericTest lessThan = (n, m) -> (n < m);
		if(lessThan.test(2, 10))
			System.out.println("Liczba 2 jest mniejsza od 10");
		if(lessThan.test(10, 2))
			System.out.println("Liczba 10 jest wieksza niz 2");
		System.out.println();
		
		NumericTest absEqual = (n, m) -> (n < 0 ? -n : n) == (m < 0 ? -m : m);
		if(absEqual.test(-4,4))
			System.out.println("Wartosci bezwzgledne -4 i 4 sa sobie rowne");
		if(absEqual.test(4, -5))
			System.out.println("Wartosci bezwzgledne nie sa sobie rowne");
		System.out.println();
	}

}
