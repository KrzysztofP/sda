interface SomeTest<T>{
	boolean test(T n, T m);
}

public class LambdaExcercise5 {
	public static void main(String[] args) {
		
		SomeTest<Integer> isFactor = (n, d) -> (n % d) == 0;
		
		if(isFactor.test(10, 2))
			System.out.println("Liczba 2 jest czynnikiem liczby 10");
		System.out.println();
		
		SomeTest<Double> isFactorD = (n, d) -> (n % d) == 0;
		
		if(isFactorD.test(212.0,  4.0))
			System.out.println("Liczba 4.0 jest czynnikiem liczby 212.0");
		System.out.println();
		
		SomeTest<String> isIn = (a, b) -> a.indexOf(b) != -1;
		
		String str = "Sparametryzowany intefejs funkcyjny";
		System.out.println("Sprawdzamy lancuch znakowy: " + str);
		
		if(isIn.test(str, "fejs"))
			System.out.println("Lancuch 'fejs' zostal znaleziony.");
		else
			System.out.println("Lancuch 'fejs' nie zostal znaleziony.");
	}
}
