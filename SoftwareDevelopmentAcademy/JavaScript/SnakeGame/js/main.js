
function Board(n, m){
    var dim1 = n;
    var dim2 = m;
    var tab = [];
    var oldSnake = null;
    
    for(var i = 0; i < dim1; i++){
        tab[i]=[];
        for(var j = 0; j < dim2; j++){
            tab[i][j] = 0;
        }
    }   
    this.drawBoard = function(){
        $('.uberDiv').remove();
        var uberDiv = $("<div class='uberDiv'/>");
        for(var i = 0; i < dim1; i++){
            var rowDiv = $("<div class='rowDiv'/>");
            for(var j = 0; j < dim2; j++){
                var div = $("<div class='boardCell'/>");
                if(tab[i][j] === 1){
                    div.addClass('isSnake');
                }
                if(tab[i][j] === 2){
                    div.addClass('isFood');
                }
                rowDiv.append(div);
            }
             uberDiv.append(rowDiv);
        }
        $('body').append(uberDiv);
    }
    this.drawSnake = function(params){
        var param = params[0];
        var result = -1;
        if(param[0] < 0 || 
           param[1] < 0 ||
           dim1 <= param[0] ||
           dim2 <= param[1]){
            result = 0;
        }
        else{
            if(tab[param[0]][param[1]] === 2){
                this.generateFood();
                result = 2;
            }
            clearSnake();
            oldSnake = [];
            for (var index = 0; index < params.length; index++){
                oldSnake.push(params[index]);
            }
            for (var index = 0; index < params.length; index++){
                tab[params[index][0]][params[index][1]] = 1;
            }
            this.drawBoard();
            if(result === -1){
                result = 1;
            }
        }
        return result;
    } 
    this.generateFood = function(){
        var found = false;
        while(!found){
            var x = Math.floor(Math.random() * dim1);
            var y = Math.floor(Math.random() * dim2);
            if(tab[x][y] === 0){
                tab[x][y] = 2; 
                found = true;
            }
        }
        this.drawBoard();
    }  
    function clearSnake(){
        if (oldSnake !== null){
            for(var index = 0; index < oldSnake.length; index++){
                tab[oldSnake[index][0]][oldSnake[index][1]] = 0;
            }
        }
    }
};

function SNAKE(){
    var position = [];
    var direction = 'DOWN';
    var grow = false;
    
    this.placeSnake = function(x1,y1){
        position.push([x1,y1]);
    }     
    this.getSnake = function(){
        return position;
    }
    this.moveSnake = function(){
        var POM = [position[0][0],position[0][1]];
        switch (direction){
            case 'DOWN': POM[0]++; break;
            case 'UP': POM[0]--; break;
            case 'LEFT': POM[1]--; break;
            case 'RIGHT': POM[1]++; break;
        }
        var POM_TAB = [];
        POM_TAB.push(POM);
        var pomLength;
        if(grow){
            pomLength = position.length;   
        }
        else{
            pomLength = position.length - 1;
        }
        for(var index = 0; index < pomLength; index++ ){
            POM_TAB.push(position[index]);
        }
        
        position = POM_TAB;
        grow = false;
    }
    this.changeDirection = function(newDirection){
        direction = newDirection;
    }
    this.growSnake = function(){
        grow = true;
    }
};

$(document).ready(function(){
    var gameBoard = new Board(15,10);
    var snake = new SNAKE();
    var myInterval;

    $('.START').click(function(){   
        snake.placeSnake(2,2);
        gameBoard.drawSnake(snake.getSnake());
        gameBoard.generateFood();
        myInterval = setInterval(function(){
            snake.moveSnake();
            var pom = gameBoard.drawSnake(snake.getSnake());
            if(pom === 0){
                clearInterval(myInterval);
                alert('przegrales');
            }
            else if(pom === 2){
                snake.growSnake();
            }
        },750)
    });
    
    $('body').keyup(function(event){
        switch (event.keyCode){
            case 37: snake.changeDirection('LEFT'); break;
            case 38: snake.changeDirection('UP'); break;
            case 39: snake.changeDirection('RIGHT'); break;
            case 40: snake.changeDirection('DOWN'); break;    
        }
    })
    $('.MOVE').click(function(){
        snake.moveSnake();
        var pom = gameBoard.drawSnake(snake.getSnake());
        if(pom === 0){
            clearInterval(myInterval);
            alert('przegrales');
        }
        else if(pom === 2){
            snake.growSnake();
        };
    })
    $('.MOVE_UP').click(function(){
        snake.changeDirection('UP');
    })
    $('.MOVE_DOWN').click(function(){
        snake.changeDirection('DOWN');
    })
    $('.MOVE_RIGHT').click(function(){
        snake.changeDirection('RIGHT');
    })
    $('.MOVE_LEFT').click(function(){
        snake.changeDirection('LEFT');
    })
});
