/* CALLBACK */
function sum(array, callback){
    var sum = 0;
    
    for(var i = 0; i < array.length; i++){
        sum+=array[i];
    }
    callback(sum);
}


/* CALLBACK2 */
function sum(array, callback){
    var sum = 0;
    
    for(var i = 0; i < array.length; i++){
        sum+=array[i];
        callback(array[i]);
    }
    return(sum);
}


/* CALLBACK3 */
function sum(array, callback){
    var sum = 0;
    
    for(var i = 0; i < array.length; i++){
        sum+=array[i];
    }
    if (typeof(callback) !== 'undefined'){
        callback(sum);
    }
    return(sum);
}


/* CALLBACK - task 1 */
function checkTable(array, callback){
    var isCorrect = true;
    
    for(var i = 0; i < array.length; i++){
        if (typeof(array[i]) !== "number" || isNaN(array[i])){
            isCorrect = false;
            break;
        }
    }
    if ( isCorrect && typeof(callback) !== 'undefined'){
        callback(array);
    }
}


/* CALLBACK - task 2 */ 
function checkTable(array, callback, callback2){
    var isCorrect = true;
    
    for(var i = 0; i < array.length; i++){
        if (typeof(array[i]) !== "number" || isNaN(array[i])){
            isCorrect = false;
            break;
        }
    }
    if ( isCorrect && typeof(callback) !== 'undefined'){
        if (array[i] < 0){
            callback1(array);
        }
        else{callback2(a)};
    }
}


/* random task */
//function forEach(array, action){
//    for(var i=0;i<array.length; i++){
//        action(array[i]);
//    }
//}

//function isDiv2(numbers){
//    var total = 0;
//    forEach(numbers, function (number){
//            if (number%2 === 0){
//                console.log(number);
//            }
//    }
//            )
//}
//isDiv2([1,2,3,4,5]);


/* Entry to objective programming */
function Pojazd(name){
    if (typeof(name)!== 'undefined'){
        this.name= name;
    }
    else{
        this.name = '';
    }
    this.speed=Math.floor(Math.random()*100);
    this.speed=0;
    this.getSpeed = function(){return this.speed;}
    this.getName = function(){return this.name;}
    this.accelerate = function(){return this.speed++;}
    this.break = function(){return this.speed=0;}
}  
//var myCar = new Pojazd('vw');
//var myCar2 = new Pojazd();
//myCar.accelerate();
//myCar.accelerate();
//console.log(myCar.getName());
//console.log(myCar.break());


function ArraySorter(array){
        if(typeof(array) !== 'undefined'){
            tab = array;
        }
        var tab;
        this.setArray = function(array){
            this.tab = array;
        }
        this.getArray = function(){
            return tab;
        }
        this.InsertSort = function(){
            var index1 = 0;
            var index2 = 0;
            var temp = 0;
            while(index1 < tab.length){
                index2 = index1 + 1;
                while(index2 < tab.length){
                if (tab[index1] > tab[index2]){
                   temp = tab[index1];
                   tab[index1] = tab[index2]; 
                   tab[index2] = temp;
                }
                index2++;
                }
                index1++;
            }
        }
        this.FillTable = function (length){
            tab = [];
            var i;
            for(i = 0; i<length; i++){
                tab[i]=parseInt(Math.random()*1000);
            }
        }
        
        this.randomArray = function(){
            this.FillTable(Math.round(Math.random()*50+50));
        }        
        this.BubbleSort = function(){
            var temp = 0;
            var index1 = 0;
            var index2 = 0;
            var FLAG = false;
                do{
                    FLAG = false;
                    index1 = 0;
                    index2 = index1 + 1;
                    if(index1 < tab.length){
                        while(index2 < tab.length){
                            if (tab[index1] > tab[index2]){
                                temp = tab[index1];
                                tab[index1] = tab[index2]; 
                                tab[index2] = temp;
                                FLAG = true;
                            }
                            index1++;
                            index2++;
                        }
                    }
                }
                while(FLAG)
                    }
}
  
        
function similar(table1, table2) {
    var POM = [];
    var INDEX1 = 0;
    var INDEX2;
    
    while (INDEX1 < table1.length) {
        if (POM.indexOf(table1[INDEX1]) < 0) {
            INDEX2 = 0;
            while (INDEX2 < table2.length) {
                if (table1[INDEX1] == table2[INDEX2]) {
                    POM.push(table1[INDEX1]);
                    break;
                }
                INDEX2++;
            }
        }
        INDEX1++;
    }
    
    return POM;
}

    
function different(tab1, tab2) {
	var simil = similar(tab1, tab2);
	var pom = [];
	for (var i = 0; i < tab1.length; i++) {
		if (simil.indexOf(tab1[i]) === -1 && pom.indexOf(tab1[i]) === -1) {
			pom.push(tab1[i]);
		}
	}
	for (var i = 0; i < tab2.length; i++) {
		if (simil.indexOf(tab2[i]) === -1 && pom.indexOf(tab2[i]) === -1) {
			pom.push(tab2[i]);
		}
	}
	return pom;
}        
        
        
function twoTabs(table1, table2){
    var tab1 = table1;
    var tab2 = table2
    var sim = null;
    var diff = null;
    
    if(typeof(table1) !== 'undefined'){
        tab1 = table1;        
    }
    if(typeof(table2) !== 'undefined'){
        tab2 = table2;
    }    
    this.getArray1 = function(){
        return tab1;
    }
    this.getArray2 = function(){
        return tab2;
    }    
    this.setArray1 = function(tab1){
        if(typeof(table1) !== 'undefined'){
        tab1 = table1;
        sim = null;
        diff = null;
        }
    return tab1;
    }
    this.setArray2 = function(tab2){
        if(typeof(table2) !== 'undefined'){
        tab2 = table2;
        sim = null;
        diff = null;
        }
    return tab2;
    }
    this.getSimilar = function(){
        if (sim === null){
            sim = similar();
        }
        return sim;
    }
    this.getDifference = function(){
        if (diff === null){
            diff = different();
        }
        return diff;
    }              
            function similar() {
                
                var POM = [];
                var INDEX1 = 0;
                var INDEX2;

                while (INDEX1 < tab1.length) {
                    if (POM.indexOf(tab1[INDEX1]) < 0) {
                        INDEX2 = 0;
                        while (INDEX2 < tab2.length) {
                            if (tab1[INDEX1] == tab2[INDEX2]) {
                                POM.push(tab1[INDEX1]);
                                break;
                            }
                            INDEX2++;
                        }
                    }
                    INDEX1++;
                }

                return POM;
            }  
            function different() {
                var simil = similar();
                var pom = [];
                
                for (var i = 0; i < tab1.length; i++) {
                    if (simil.indexOf(tab1[i]) === -1 && pom.indexOf(tab1[i]) === -1) {
                        pom.push(tab1[i]);
                    }
                }
                for (var i = 0; i < tab2.length; i++) {
                    if (simil.indexOf(tab2[i]) === -1 && pom.indexOf(tab2[i]) === -1) {
                        pom.push(tab2[i]);
                    }
                }
                return pom;
            }     
        }
//    var zadanie = new twoTabs([1,2,7878,4,4,5],[1,2,3,3,4,5]);
//   
//    console.log(zadanie.getSimilar());
//    console.log(zadanie.getDifference());