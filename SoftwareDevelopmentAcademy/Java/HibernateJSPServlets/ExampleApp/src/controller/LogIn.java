package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Servlet implementation class LogIn
 */
@WebServlet("/LogIn")
public class LogIn extends HttpServlet {
	private static final long serialVersionUID = 1L;

	SessionFactory sFactory;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogIn() {
		try {
			this.sFactory = new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// request.getRequestDispatcher("welcome.jsp").forward(request,
		// response);
		String akcja = request.getParameter("akcja");
		HttpSession ses = request.getSession();
		if (akcja.equalsIgnoreCase("wyloguj")) {
			ses.removeAttribute("login");
			ses.removeAttribute("password");
			response.sendRedirect("index.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// logowanie
		String akcja = request.getParameter("akcja");
		if (akcja.equalsIgnoreCase("logowanie")) {
			String loginS = request.getParameter("login");
			String passwordS = request.getParameter("password");
			ControllerLogic cl = new ControllerLogic(loginS, passwordS);
			boolean tryLog = cl.isLoggedIn();

			HttpSession ses = request.getSession();

			if (tryLog) {
				ses.setAttribute("login", loginS);
				ses.setAttribute("password", passwordS);
				response.sendRedirect("welcome.jsp");
			} else {
				response.sendRedirect("error.jsp");
			}
		}
	}

}
