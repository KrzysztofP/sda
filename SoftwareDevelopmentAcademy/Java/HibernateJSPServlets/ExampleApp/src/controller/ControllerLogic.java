package controller;

public class ControllerLogic {
	private String login;
	private String password;

	public ControllerLogic() {
	}

	public ControllerLogic(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLoggedIn() {
		if (this.login.equals("a") && this.password.equalsIgnoreCase("aa")) {
			return true;
		}
		return false;
	}

}
