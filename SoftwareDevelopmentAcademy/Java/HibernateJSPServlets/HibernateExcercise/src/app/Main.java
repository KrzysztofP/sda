package app;

import java.util.Scanner;
import java.util.logging.Level;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.sql.Template;

import antlr.collections.List;

public class Main {

	public static SessionFactory sf;

	public static void main(String[] args) {
		System.out.println("Connecting to a database...");
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
		try {
			Main.sf = new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Connection successfull.");
		System.out.println(
				"     MENU: \n =========== \n 1. Przejrzyj rekordy \n 2. Dodaj rekord \n 3. Zmodyfikuj rekord \n 4. Usu� rekord \n 5. Zako�cz dzia�anie programu");
		
		String choice;
		Scanner mainScanner = new Scanner(System.in);

		System.out.print("Please enter a number from menu: ");
		choice = mainScanner.next();

		CarsController sc = new CarsController();
		Cars sm = new Cars(1, "Audi", "RS6", 2015);
		Cars sm2 = new Cars(2, "VW", "Das Auto", 2000);
		Cars sm3 = new Cars(3, "Fiat", "126p", 1978);
		
		while (choice != "5") {
			switch (choice) {
			case "1":
				sc.getRecord();
				break;
			case "2":
				int ids;
				String marka;
				String model;
				int rok_prod;
				CarsController cc = new CarsController();
				Scanner caseTwoScannerIds = new Scanner(System.in);
				System.out.print("Please enter a number of elements: ");
				ids = Integer.parseInt( caseTwoScannerIds.next() );
				int temp_ids = ids;
				for(int index = 0; index < ids; index++){
					temp_ids =+ 1;
					Scanner caseTwoScannerFor = new Scanner(System.in);
					System.out.print("Please enter a marque of car, " + temp_ids + " entity:");
					marka = mainScanner.next();
					System.out.print("Please enter a model of car, " + temp_ids + " entity:");
					model = mainScanner.next();
					System.out.print("Please enter a production date of car, " + temp_ids + " entity:");
					rok_prod = Integer.parseInt( mainScanner.next());
					Cars car = new Cars(ids, marka, model, rok_prod);
					sc.addRecord(car);
					sc.getRecord();
				}
				System.exit(0);
				break;
			case "3":
				sc.modifyRecord(2);
				break;
			case "4":
				sc.removeRecord(2);
				break;
			case "5":
				System.out.println("See you in another live, brother.");
				System.exit(0);
				break;
			}
		}

		sc.addRecord(sm);
		sc.addRecord(sm2);
		sc.addRecord(sm3);
		sc.getRecord();

		sc.modifyRecord(2);
		sc.getRecord();

		sc.removeRecord(2);
		sc.getRecord();

		System.exit(0);
	}

}
