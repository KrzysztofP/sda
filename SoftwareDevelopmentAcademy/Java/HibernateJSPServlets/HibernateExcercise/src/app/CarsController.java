package app;

import java.util.Iterator;
import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class CarsController {

	public void addRecord(Cars car) {
		Session s = Main.sf.openSession();
		Transaction t = s.beginTransaction();

		try {
			s.save(car);
			t.commit();
		} catch (Exception e) {
			if (t != null)
				t.rollback();
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
	}

	public void getRecord() {
		Session s = Main.sf.openSession();
		Transaction t = s.beginTransaction();

		s.setCacheMode(CacheMode.IGNORE);
		try {
			List dane = s.createQuery("FROM Cars").list();
			for (Iterator i = dane.iterator(); i.hasNext();) {
				Cars ph_temp = (Cars) i.next();
				System.out.println("ID: " + ph_temp.getId() + " | Marka: " + ph_temp.getMarka() + " | Model: "
						+ ph_temp.getModel() + " | Rok produkcji: " + ph_temp.getRok_produkcji());
			}
		} catch (Exception e) {

		} finally {
			s.close();
		}
	}

	public void modifyRecord(int id) {
		Session s = Main.sf.openSession();
		Transaction t =  null;
		try {
			t = s.beginTransaction();
			Cars sm  = (Cars) s.get(Cars.class, id);
			sm.setRok_produkcji(1966);
			s.update(sm);
			t.commit();
		} catch (Exception e) {

		} finally {
			s.close();
		}
	}
	
	public void removeRecord(int id) {
		Session s = Main.sf.openSession();
		Transaction t =  null;
		try {
			t = s.beginTransaction();
			Cars sm  = (Cars) s.get(Cars.class, id);
			s.delete(sm);
			t.commit();
		} catch (Exception e) {

		} finally {
			s.close();
		}
	}
}
