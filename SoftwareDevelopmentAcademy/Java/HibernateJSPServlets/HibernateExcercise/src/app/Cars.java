package app;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cars")
public class Cars {
	@Id
	private int id;
	private String marka;
	private String model;
	private int rok_produkcji;

	public Cars() {
	}

	public Cars(int id, String marka, String model, int rok_produkcji) {
		super();
		this.id = id;
		this.marka = marka;
		this.model = model;
		this.rok_produkcji = rok_produkcji;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getRok_produkcji() {
		return rok_produkcji;
	}

	public void setRok_produkcji(int rok_produkcji) {
		this.rok_produkcji = rok_produkcji;
	}

	@Override
	public String toString() {
		return "Cars [id=" + id + ", marka=" + marka + ", model=" + model + ", rok_produkcji=" + rok_produkcji + "]";
	}
	
	

}
