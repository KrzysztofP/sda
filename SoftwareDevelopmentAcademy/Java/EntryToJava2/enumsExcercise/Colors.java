package enumsExcercise;

public enum Colors {

	RED(255, 0, 0), YELLOW(255, 255, 0), GREEN(0, 255, 0) {
		@Override
		public String toString() {
			return "Jestem czyms nadrzednym. ";
		}
	};

	private int red;
	private int green;
	private int blue;

	private Colors(int red, int green, int blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public int getRed() {
		return red;
	}

	public int getGreen() {
		return green;
	}

	public int getBlue() {
		return blue;
	}

	@Override
	public String toString() {
		return "Moje kolory to: " + red + "," + green + "," + blue;
	}

}
