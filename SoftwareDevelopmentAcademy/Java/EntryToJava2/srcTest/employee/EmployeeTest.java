package employee;

import org.junit.Assert;
import org.junit.Test;

public class EmployeeTest {

	@Test
	public void shouldReturnTrueIfEmployeesSameName() {
		// given
		Employee e1 = new Employee("Krzysztof", "P", 27, 10);
		Employee e2 = new Employee("Krzysztof", "Inny", 30, 5);

		// when
		boolean result = e1.equals(e2);

		// then
		Assert.assertEquals(true, result);
	}

	@Test
	public void shouldReturnTrueIfEmployeesSameSurname() {
		// given
		Employee e1 = new Employee("Krzysztof", "P", 27, 10);
		Employee e2 = new Employee("Marek", "P", 30, 10);

		// when
		boolean result = e1.equals(e2);

		// then
		Assert.assertEquals(true, result);
	}

	@Test
	public void shouldReturnTrueIfEmployeesSameSeniority() {
		// given
		Employee e1 = new Employee("Krzysztof", "P", 27, 10);
		Employee e2 = new Employee("Krzysztof", "Inny", 30, 10);

		// when
		boolean result = e1.equals(e2);

		// then
		Assert.assertEquals(true, result);
	}
}
