package hashEqualsExcercise;

import org.junit.Assert;
import org.junit.Test;

public class PointTest {

	@Test
	public void shouldReturnTrueForTwoPoints() {
		//given
		Point p1 = new Point(1, 8);
		Point p2 = new Point(1, 8);
		
		//when
		boolean result = p1.equals(p2);
		
		//then
		Assert.assertEquals(true, result);
	}
	
	@Test
	public void shouldReturnFalseForTwoNotEqualPoints() {
		//given
		Point p1 = new Point(1, 8);
		Point p2 = new Point(2, 8);
		
		//when
		boolean result = p1.equals(p2);
		
		//then
		Assert.assertEquals(false, result);
	}
	

}
