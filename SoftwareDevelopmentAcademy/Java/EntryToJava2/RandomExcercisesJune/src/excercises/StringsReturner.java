package excercises;

public class StringsReturner {
	private String asci;

	public void generateCharacters() {
		String asci = "";

		for (int i = 97; i <= 122; i++) {
			this.asci += (char) i;
		}
		System.out.println(asci);
	}

	public void findMissingLetters(String firstString, String secondString) {
		String temp = firstString + secondString;

		for (int i = 0; i < this.asci.length(); i++) {
			boolean isLetter = false;

			for (int j = 0; j < temp.length(); j++) {
				if (this.asci.charAt(i) == temp.charAt(j)) {
					isLetter = true;
				}
			}
			if (!isLetter) {
				System.out.print(asci.charAt(i) + ", ");
			}
		}
	}

	public void findSameCharacters(String firstString, String secondString) {

		for (int i = 0; i < firstString.length(); i++) {
			boolean isLetter = false;

			for (int j = 0; j < secondString.length(); j++) {
				if (firstString.charAt(i) == secondString.charAt(j)) {
					isLetter = true;
				}
			}
			if (isLetter) {
				System.out.print(firstString.charAt(i) + ", ");
			}
		}
	}

	public void findDifferentCharacters(String firstString, String secondString, boolean breakingFlag) {

		for (int i = 0; i < firstString.length(); i++) {
			boolean isLetter = false;

			for (int j = 0; j < secondString.length(); j++) {
				if (firstString.charAt(i) == secondString.charAt(j)) {
					isLetter = true;
				}
			}
			if (!isLetter) {
				System.out.print(firstString.charAt(i) + ", ");
			}
		}
		if (breakingFlag == false) {
			findDifferentCharacters(secondString, firstString, true);
		}
	}
}
