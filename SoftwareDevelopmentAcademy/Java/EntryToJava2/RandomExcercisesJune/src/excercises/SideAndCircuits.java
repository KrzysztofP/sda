package excercises;

import javafx.geometry.Side;

public class SideAndCircuits {
	private Double a, b, c, area, circumference;

	public SideAndCircuits(Double a, Double b) {
		this.a = a;
		this.b = b;
		this.c = null;
		this.area = this.area();
		this.circumference = this.circumference();
	}

	public SideAndCircuits(Double a, Double b, Double c) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.area = this.area();
		this.circumference = this.circumference();
	}

	public Double area() {
		Double area = 0.0;

		if (this.c == null) {
			area = a * b;
		} else {
			Double p = 0.5 * (this.a + this.b + this.c);
			area = Math.sqrt(p * (p - this.a) * (p - this.b) * (p - this.c));
		}

		return area;
	}

	public Double circumference(){
		Double circumference = 0.0;
		if(this.c == null){
			circumference=2*a+2*b;
		} else{
			circumference = this.a + this.b + this.c;
		}
		
		return circumference;
	}

	@Override
	public String toString() {
		return "SideAndCircuits [a=" + a + ", b=" + b + ", c=" + c + ", area=" + area + ", circumference="
				+ circumference + "]";
	}
	
	
	
}
