package excercises;

public class Settlement {

	private void addresses(String streetName, int lastBlockNumber) {

		if (lastBlockNumber % 2 == 0) {
			lastBlockNumber--;
		}

		for (int i = 1; i <= lastBlockNumber; i += 2) {
			String staircase = "";

			for (int j = 1; j < 12; j++) {
				if (j < 7) {
					staircase = "A";
				} else {
					staircase = "B";
				}
				System.out.println("ul." + streetName + " " + i + "/" + staircase + "/" + j);
			}

		}
	}
}
