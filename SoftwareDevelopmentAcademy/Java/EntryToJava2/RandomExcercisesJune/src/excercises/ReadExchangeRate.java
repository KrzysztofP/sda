package excercises;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class ReadExchangeRate {
	private String file;
	private HashMap<String, Double> exchangeRates;

	public ReadExchangeRate(String file) {
		this.file = file;
		this.exchangeRates = new HashMap<String, Double>();
	}

	public void getFileContent() {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(this.file));
			String line = "";
			line = bufferedReader.readLine();
			while (line != null) {
				String[] keyValueTab = line.split("\t");
				this.exchangeRates.put(keyValueTab[0], Double.parseDouble(keyValueTab[1].replace(",", ".")));
				line = bufferedReader.readLine();
				System.out.println(keyValueTab[0] + " " + Double.parseDouble(keyValueTab[1].replace(",", ".")));
			}
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			this.exchangeRates.put("PLN", 1.0);
		}
	}

}
