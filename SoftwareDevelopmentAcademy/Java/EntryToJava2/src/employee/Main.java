package employee;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<Employee> employees = Arrays.asList(new Employee("Krzysztof", "P", 15, 10),
				new Employee("Marek", "P", 15, 10), new Employee("Olgierd", "P", 15, 10),
				new Employee("Krzysztof", "P", 20, 10));

		System.out.println(employees);
		Collections.sort(employees);
		System.out.println(employees);

	}

}
