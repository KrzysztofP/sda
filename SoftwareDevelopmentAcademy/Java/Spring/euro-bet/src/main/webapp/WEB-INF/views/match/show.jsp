<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<body>

	<table>
		<tr>
			<td>Match - time</td>
			<td>Host</td>
			<td>Visitor</td>
			<td></td>
			<td>Result</td>
			<td></td>
		</tr>
		<c:forEach var="match" items="${matches}">
			<tr>
				<td>${match.matchTime}</td>
				<td>${match.hostTeam.name}</td>
				<td>${match.guestTeam.name}</td>
				<td>${match.hostScore}</td>
				<td>:</td>
				<td>${match.guestScore}</td>
				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<td><button>Remove match</button></td>
					<td><button>Edit result</button></td>
				</sec:authorize>
			</tr>
		</c:forEach>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<td><button>Add match</button></td>
			</sec:authorize>
		</tr>
	</table>

</body>
</html>