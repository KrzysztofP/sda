<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<body>

	<table>
		<tr>
			<td>Login</td>
			<td>Full Name</td>
			<td>Role</td>
			<td>Total Result</td>
			<td>Action</td>
		</tr>
		<c:forEach var="user" items="${users}">
			<tr>
				<td>${user.login}</td>
				<td>${user.fullName}</td>
				<td>${user.role}</td>
				<td>UserResult TODO</td>
				<td><button>Remove user</button></td>
				<td><button>Change password</button></td>
			</tr>
		</c:forEach>
	</table>

</body>
</html>