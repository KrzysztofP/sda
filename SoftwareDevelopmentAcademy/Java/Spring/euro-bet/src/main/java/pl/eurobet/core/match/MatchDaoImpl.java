package pl.eurobet.core.match;

import org.springframework.stereotype.Repository;

import pl.eurobet.core.AbstractBaseDao;

@Repository
public class MatchDaoImpl extends AbstractBaseDao<Match, Long> implements MatchDao {

	@Override
	protected Class<Match> supports() {
		return Match.class;
	}

}
