package pl.eurobet.core.team;

import org.springframework.stereotype.Repository;

import pl.eurobet.core.AbstractBaseDao;

@Repository
public class TeamDaoImpl extends AbstractBaseDao<Team, Long> implements TeamDao {

	@Override
	protected Class<Team> supports() {
		return Team.class;
	}

}
