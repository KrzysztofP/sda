package pl.eurobet.core.bet;

import pl.eurobet.core.BaseDao;

public interface BetDao extends BaseDao<Bet, Long> {

}
