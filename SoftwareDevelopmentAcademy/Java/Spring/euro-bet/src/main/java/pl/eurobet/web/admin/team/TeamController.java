package pl.eurobet.web.admin.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pl.eurobet.core.team.TeamDao;

@Controller
public class TeamController {
	
	@Autowired
	private TeamDao teamDao;
	
	@RequestMapping("/admin/teams")
	public ModelAndView showTeamList() {
		ModelAndView modelAndView = new ModelAndView("team/show");
		modelAndView.addObject("teams", teamDao.getAll());
		return modelAndView;				
	}

}
