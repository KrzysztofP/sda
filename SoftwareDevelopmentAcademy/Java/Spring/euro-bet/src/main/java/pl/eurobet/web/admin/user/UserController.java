package pl.eurobet.web.admin.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pl.eurobet.core.user.UserDao;

@Controller
public class UserController {

	@Autowired
	private UserDao userDao;

	@RequestMapping("/admin/users")
	public ModelAndView showUserList() {
		ModelAndView modelAndView = new ModelAndView("user/show");
		modelAndView.addObject("users", userDao.getAll());
		return modelAndView;
	}

}
