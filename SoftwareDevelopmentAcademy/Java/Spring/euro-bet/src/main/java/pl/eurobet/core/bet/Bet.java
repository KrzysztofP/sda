package pl.eurobet.core.bet;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import pl.eurobet.core.match.Match;
import pl.eurobet.core.user.User;

@Entity
public class Bet {
	@Id
	@GeneratedValue(generator = "betSeq")
	@SequenceGenerator(name = "betSeq", sequenceName = "bet_seq")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToOne
	@JoinColumn(name = "match_id")
	private Match match;

	private int guestScore;
	private int hostScore;
	private Date betTime;
	private int points;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Match getMatch() {
		return match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public int getGuestScore() {
		return guestScore;
	}

	public void setGuestScore(int guestScore) {
		this.guestScore = guestScore;
	}

	public int getHostScore() {
		return hostScore;
	}

	public void setHostScore(int hostScore) {
		this.hostScore = hostScore;
	}

	public Date getBetTime() {
		return betTime;
	}

	public void setBetTime(Date betTime) {
		this.betTime = betTime;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "Bet [id=" + id + ", user=" + user + ", match=" + match + ", guestScore=" + guestScore + ", hostScore="
				+ hostScore + ", betTime=" + betTime + ", points=" + points + "]";
	}

}
