package pl.eurobet.core.user;

public interface UserProvider {
	
	User getLoggedUser();
	
	void saveLoggedUser(String username);

}
