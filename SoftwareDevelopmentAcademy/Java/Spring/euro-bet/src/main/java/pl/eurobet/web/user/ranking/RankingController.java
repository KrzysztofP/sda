package pl.eurobet.web.user.ranking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pl.eurobet.core.match.MatchDao;

@Controller
public class RankingController {
	
	@Autowired
	private MatchDao matchDao;
	
	@RequestMapping("/user/scores")
	public ModelAndView showRankingList() {
		ModelAndView modelAndView = new ModelAndView("score/show");
		modelAndView.addObject("scores", matchDao.getAll());
		return modelAndView;				
	}

}
