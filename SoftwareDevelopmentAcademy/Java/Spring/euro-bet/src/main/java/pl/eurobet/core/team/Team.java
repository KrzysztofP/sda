package pl.eurobet.core.team;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Team {
	
	@Id
	@GeneratedValue(generator = "teamSeq")
	@SequenceGenerator(name = "teamSeq", sequenceName = "team_seq")
	private Long id;
	
	@Column
	private String name;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

}
