package pl.eurobet.core.bet;

import org.springframework.stereotype.Repository;

import pl.eurobet.core.AbstractBaseDao;

@Repository
public class BetDaoImpl extends AbstractBaseDao<Bet, Long> implements BetDao {

	@Override
	protected Class<Bet> supports() {
		return Bet.class;
	}

}
