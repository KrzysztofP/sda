package pl.eurobet.web.user.match;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pl.eurobet.core.match.Match;
import pl.eurobet.core.match.MatchDao;

@Controller
public class MatchController {
	
	@Autowired
	private MatchDao matchDao;
	
	@RequestMapping("/user/matches")
	public ModelAndView showMatchesList() {
		ModelAndView modelAndView = new ModelAndView("match/show");
		List<Match> matches = matchDao.getAll();
		Collections.sort(matches);
		
		modelAndView.addObject("matches", matches);
		return modelAndView;				
	}

}
