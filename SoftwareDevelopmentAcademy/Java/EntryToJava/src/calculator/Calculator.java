package calculator;

public class Calculator {

	static int add(int a, int b) {
		System.out.println("adding int");
		return a + b;
	}

	static float add(float a, float b) {
		System.out.println("adding floats");
		return a + b;
	}

	static boolean add(boolean a, boolean b) {
		System.out.println("adding booleans");
		return a | b;
	}
}
