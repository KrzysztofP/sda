
public class Square implements FigureInterface{
	private double a;
	
	public Square(double a) {
		super();
		this.a = a;
	}

	@Override
	public double calculateSurface() {
		return a*a;
	}

	@Override
	public double calculatePerimeter() {
		return 4 * a;
	}
}