package randomExcercise;

public class Main {

	public static void main(String[] args) {
		String first = "text";

		String second = new String("text");

		System.out.println(first == "text");

		System.out.println(second == "text");

		System.out.println(first.equals("text"));

		System.out.println(second.equals("text"));

		System.out.println(first == second);

		System.out.println(first.equals(second));

	}

}
