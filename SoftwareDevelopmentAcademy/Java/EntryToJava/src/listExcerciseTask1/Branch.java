package listExcerciseTask1;

public class Branch {
	private String name;
	private long employeesNumber;
	
	public Branch(String name, long employeesNumber) {
		super();
		this.name = name;
		this.employeesNumber = employeesNumber;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getEmployeesNumber() {
		return employeesNumber;
	}
	public void setEmployeesNumber(long employeesNumber) {
		this.employeesNumber = employeesNumber;
	}

	@Override
	public String toString() {
		return super.toString();
	}
	// TODO
	
}
