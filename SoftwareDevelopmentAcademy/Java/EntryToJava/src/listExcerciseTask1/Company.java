package listExcerciseTask1;

import java.util.Iterator;
import java.util.List;

public class Company implements Iterable<Branch> {
	private List<Branch> listBranches;

	@Override
	public Iterator<Branch> iterator() {
		return listBranches.iterator();
	}

	public void displayAllBranches(List<Branch> listBranches) {
		Iterator<Branch> iterator = listBranches.iterator();
		while (iterator.hasNext()) {
			Branch numer = iterator.next();
			System.out.println(numer);
		}
	}
	// TODO
}
