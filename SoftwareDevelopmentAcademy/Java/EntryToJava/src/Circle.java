
public class Circle implements FigureInterface{
	private double r;
	
	public Circle(double r) {
		super();
		this.r = r;
	}

	@Override
	public double calculateSurface() {
		return Math.PI * r * r;
	}

	@Override
	public double calculatePerimeter() {
		// TODO Auto-generated method stub
		return 0;
	}
}