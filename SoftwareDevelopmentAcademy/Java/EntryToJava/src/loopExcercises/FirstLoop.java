package loopExcercises;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FirstLoop {

	public static void main(String[] args) throws IOException {
		
		BufferedReader reader = new BufferedReader(new FileReader("file_name_random"));
		String lineFromFile = null;
		while((lineFromFile = reader.readLine()) != null){
			switch(lineFromFile){
			case "0": System.out.println("Zero"); break;
			case "1": System.out.println("One"); break;
			case "2": System.out.println("Two"); break;
			case "3": System.out.println("Three"); break;
			case "4": System.out.println("Four"); break;
			case "5": System.out.println("Five"); break;
			case "6": System.out.println("Six"); break;
			}
			System.out.println(lineFromFile);
			
			
		}
	}
}
