
public abstract class Czworobok implements FigureInterface{
	private double sideA;
	private double sideB;
	private double sideC;
	private double sideD;
	
	public Czworobok(double sideA, double sideB, double sideC, double sideD) {
		super();
		this.sideA = sideA;
		this.sideB = sideB;
		this.sideC = sideC;
		this.sideD = sideD;
	}
	
	public double perimeter(){
		return sideA + sideB + sideC + sideD;
	}
	
	//TODO
	// kolo po FigureInterface
	// czworobok po FigInt
	// romb po czroroboku
	// rownoleglobok po czwororboku
	// kwadrat po rombie
}
