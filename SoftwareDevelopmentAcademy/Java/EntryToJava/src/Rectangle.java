
public class Rectangle implements FigureInterface{
	private double a;
	private double b;

	public Rectangle(double a, double b) {
		super();
		this.a = a;
		this.b = b;
	}
	
	@Override
	public double calculateSurface() {
		return a * b;
	}

	@Override
	public double calculatePerimeter() {
		// TODO Auto-generated method stub
		return 0;
	}
}