

public class Figures {
	public static void main(String[] args) {
		
		FigureInterface[] myFigures = new FigureInterface[3];
		
		myFigures[0] = new Square(10);
		myFigures[1] = new Circle(10);
		myFigures[2] = new Rectangle(10, 5);
		
		for(int i = 0; i < myFigures.length; i++){
			System.out.println("Field od my figure: " + myFigures[i].calculateSurface());
		}

	}


}
