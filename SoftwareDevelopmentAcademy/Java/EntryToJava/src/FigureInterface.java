
public interface FigureInterface {
	double calculateSurface();
	double calculatePerimeter();
}