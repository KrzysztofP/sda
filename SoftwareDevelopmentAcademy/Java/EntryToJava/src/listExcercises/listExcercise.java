package listExcercises;

import java.util.*;

public class listExcercise {

	public static void divideAndDisplay(List<Integer> listToChange) {

		Integer number = listToChange.get(0);

		for (Iterator<Integer> i = listToChange.iterator(); i.hasNext(); number = i.next()) {
			if ((number % 5) == 0)
				i.remove();
		}
		System.out.println("Po usunieciu podzielnych przez 5: " + listToChange);
		
		number = listToChange.get(0);

		for (Iterator<Integer> i = listToChange.iterator(); i.hasNext(); number = i.next()) {
			if ((number % 2) == 0) {
				System.out.println(number);
			}
		}
		
		System.out.println(number);
	}

	public static void main(String[] args) {
		List<Integer> listToChange = new ArrayList<>(Arrays.asList(1, -2, 8, 1, 5, 15, 22));
		divideAndDisplay(listToChange);
	}

}
