package listsExcercise2;


public class Person implements Comparable<Person>{

	public String name;
	public String surname;
	public String company;


	public Person(String name, String surname, String company) {
		super();
		this.name = name;
		this.surname = surname;
		this.company = company;
	}

	@Override
	public int compareTo(Person o) {
		return this.surname.compareTo(o.surname);
	}

	@Override
	public String toString() {
		return "/nPerson [surname=" + surname + ", name=" + name + ", company" + company +"]";
	}
	
	public static void treeSetPerson(){
		//TODO
	}
	

}
