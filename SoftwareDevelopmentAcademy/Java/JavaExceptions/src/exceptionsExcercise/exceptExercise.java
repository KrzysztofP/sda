package exceptionsExcercise;

public class exceptExercise {

	public static void main(String[] args) throws Exception {

		strLen(null);

	}
	
	public static void strLen(String s) throws Exception {

		try {
			System.out.println(s.length());
		} catch (NullPointerException e) {
			System.out.println("Caught exception");
			e.printStackTrace();
			e.fillInStackTrace();
			throw new Exception("Caused by: ");
			// Task: "7. Do��cz obs�ugiwany wyj�tek do nowo tworzonego wyj�tku
			// Exception jako przyczyn� jego powstania."
			// 'throw new Exception();' zmienione na 'throw new Exception(e);'
		}
	}
}
