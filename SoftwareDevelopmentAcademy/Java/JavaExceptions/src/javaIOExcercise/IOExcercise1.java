package javaIOExcercise;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

public class IOExcercise1 {

	public static void main(String[] args) throws WrongPathException {
		Set<String> myStrings = new TreeSet<String>();
		// myStrings.add("Afirst element\n");
		// myStrings.add("Csecond element\n");
		// myStrings.add("Bthird element\n");
		// myStrings.add("Dfourth element\n");
		//
		// randomWriter(myStrings, "file_name_random");

		randomWriter(myStrings, "file_name_random");

	}

	public static void randomWriter(Set<String> setStringwriter, String pathFile) throws WrongPathException {
		File file = new File(pathFile);
		if (!file.exists()) {
			throw new WrongPathException("to jest moj komunikat wyjatku!!!!!!!!!!");
		}

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(pathFile))) {
			for (String myString : setStringwriter) {
				writer.write(myString);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void randomReader(Set<String> setStringwriter, String PathFile) throws WrongPathException {
		try (BufferedReader reader = new BufferedReader(new FileReader(PathFile))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {

			throw new WrongPathException();
		}
	}

}
