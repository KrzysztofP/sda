package listsExcercise;

import java.util.ArrayList;
import java.util.List;

public class lessThanAverage {

	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<>();
		myList.add(0);
		myList.add(0);
		myList.add(0);
		myList.add(0);
		myList.add(0);
		myList.add(0);
		myList.add(1);

		System.out.println(myList);
		System.out.println(returnLessThanAverageList(myList));

	}

	public static double returnAverage(List<Integer> myList) {
		double avg = 0;

		for (Integer listElement : myList) {
			avg += listElement;
		}
		avg = avg / myList.size();
		return avg;
	};

	public static List<Integer> returnLessThanAverageList(List<Integer> myList) {
		List<Integer> lessThanList = new ArrayList<>();

		double avg = returnAverage(myList);

		for (Integer listElement : myList) {
			if (listElement < avg) {
				lessThanList.add(listElement);
			}
		}
		return lessThanList;
	};
}
