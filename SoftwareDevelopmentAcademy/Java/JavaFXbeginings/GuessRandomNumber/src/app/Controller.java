package app;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
	@FXML
	private Label info;
	@FXML
	private TextField userInput;
	@FXML
	private Button guess;

	private int number;

	public Controller() {
		this.number = (int) (Math.random() * 100);
		System.out.println(number);
	}

	@FXML
	public void guessNumber(ActionEvent ae) {
		this.info.setText("Number has been drawn");
		int userValue = Integer.parseInt(this.userInput.getText());
		if (userValue > this.number) {
			System.out.println("Your number was too big");
		} else if (userValue < this.number) {
			System.out.println("Your number was too small");
		} else {
			System.out.println("Your number is equal. Congratulations!");
		}
	}

}
