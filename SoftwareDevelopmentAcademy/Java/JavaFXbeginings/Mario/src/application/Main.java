package application;

import javafx.scene.image.Image;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public class Main extends Application {
	final static int width = 600;
	final static int height = 600;
	public static int marioPosX;
	public static int marioPosY;

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Mario");

		Group root = new Group();
		Scene theScene = new Scene(root);
		primaryStage.setScene(theScene);
		Canvas canvas = new Canvas(Main.width, Main.height);

		root.getChildren().add(canvas);

		GraphicsContext gContext = canvas.getGraphicsContext2D();
		primaryStage.show();

		AnimatedImage marioMovement = new AnimatedImage();

		Image[] imageArray = new Image[4];
		imageArray[0] = new Image("/resources/0.png");
		imageArray[1] = new Image("/resources/1.png");
		imageArray[2] = new Image("/resources/0.png");
		imageArray[3] = new Image("/resources/1.png");
		
		Image backgroundImage = new Image("/resources/grass.png");

		marioMovement.setFrames(imageArray);
		marioMovement.setDuration(0.01);
		
		MovementLeftRight move = new MovementLeftRight(theScene);

		final long startNanoTime = System.nanoTime();
		marioPosX = Main.width / 2 - (int) (marioMovement.getFrame(0).getWidth() / 2);
		marioPosY = Main.height / 2 - (int) (marioMovement.getFrame(0).getWidth() / 2) - 40;
		
		new AnimationTimer() {

			@Override
			public void handle(long now) {
				double t = (now - startNanoTime) / Math.pow(10, 9);

				gContext.clearRect(0, 0, Main.width, Main.height);
				gContext.drawImage(backgroundImage, 0, 0);
				gContext.drawImage(marioMovement.getFrame(t), marioPosX, marioPosY);

			}
		}.start();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
