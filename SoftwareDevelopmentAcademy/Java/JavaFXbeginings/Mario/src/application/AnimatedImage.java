package application;

import javafx.scene.image.Image;

public class AnimatedImage {
	private Image[] frames;
	private double duration;
	static boolean isAnim = false;
	static boolean moveLeft = false;
	static boolean moveRight = false;

	public Image[] getFrames() {
		return frames;
	}

	public void setFrames(Image[] frames) {
		this.frames = frames;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public Image getFrame(double time) {
		int index = (int) ((time % (frames.length / 2 * duration)) / duration);
		if (AnimatedImage.isAnim == false) {
			if(moveRight == false){
				index = 3;
			} else{
				index = 0;
			}
		} else {
			if (moveRight == false) {
				index =+ 2;
			}
		}
		return frames[index];
	}
}