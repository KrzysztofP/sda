package application;

import java.util.HashSet;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.*;

public class MovementLeftRight {

	HashSet<String> currentActiveKey = new HashSet<String>();
	Scene moveLeftRightScene;

	public MovementLeftRight(Scene moveLeftRightScene) {
		this.moveLeftRightScene = moveLeftRightScene;
		this.addMarioEvents();
	}

	public void addMarioEvents() {
		this.moveLeftRightScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent arg0) {
				currentActiveKey.add(arg0.getCode().toString());
				AnimatedImage.isAnim = true;
				if (currentActiveKey.contains("LEFT")) {
					Main.marioPosX -= 20;
					AnimatedImage.moveRight = false;
				} else if (currentActiveKey.contains("RIGHT")) {
					Main.marioPosX += 20;
					AnimatedImage.moveRight = true;
				} else{
					AnimatedImage.isAnim = false;
				}
			}
		});

		this.moveLeftRightScene.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent arg0) {
				currentActiveKey.remove(arg0.getCode().toString());
				AnimatedImage.isAnim = false;
			}
		});

	}

	public static void main(String[] args) {

	}

}
