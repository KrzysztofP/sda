package com.printticket;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FilterInputStream;

import javax.swing.*;
import javax.swing.text.StyledEditorKit.ItalicAction;

public class GenerateTicketForm extends Frame {

	Panel panelTop;

	Label dateLabel;
	TextField date;
	Label timeLabel;
	TextField time;
	Label numberLabel;
	TextField number;
	Label numberRefLabel;
	TextField numberRef;
	Label branchLabel;
	TextField branch;

	Panel panelBottom;

	Button generate;

	public GenerateTicketForm() {
		setTitle("Generating Ticket");
		setBounds(200, 200, 600, 800);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		panelTop = new Panel();
		panelTop.setLayout(new GridLayout(6, 2));
		dateLabel = new Label("Data:");
		timeLabel = new Label("Godzina: ");
		numberLabel = new Label("Numer: ");
		numberRefLabel = new Label("Numer Ref.: ");
		branchLabel = new Label("Oddzial:");

		date = new TextField();
		time = new TextField();
		number = new TextField();
		numberRef = new TextField();
		branch = new TextField();

		panelBottom = new Panel();
		panelBottom.setLayout(new FlowLayout());
		generate = new Button("Generuj");
		generate.setFont(new Font("Arial", 10, 150));

		add(panelTop);
		panelTop.add(dateLabel);
		panelTop.add(date);
		panelTop.add(timeLabel);
		panelTop.add(time);
		panelTop.add(numberLabel);
		panelTop.add(number);
		panelTop.add(numberRefLabel);
		panelTop.add(numberRef);
		panelTop.add(branchLabel);
		panelTop.add(branch);

		add(panelBottom);
		panelBottom.add(generate);
		generate.addActionListener(new ButtonListener(this));
		setVisible(true);

		addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
			}
		});
	}

	public static void main(String[] args) {
		new GenerateTicketForm();

	}
}
