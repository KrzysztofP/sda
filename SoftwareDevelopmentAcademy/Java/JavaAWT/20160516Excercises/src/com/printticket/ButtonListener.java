package com.printticket;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener {

	GenerateTicketForm listening;
	public ButtonListener(GenerateTicketForm listening) {
		super();
		this.listening = listening;
	}
	@Override
	public void actionPerformed(ActionEvent e) {

		new Result(this.listening);

	}

}
