package com.printticket;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;

public class Result extends Frame {
	GenerateTicketForm myFrame;

	public Result(GenerateTicketForm listening) {
		this.myFrame = listening;
		setTitle("BILET");
		setBounds(250, 250, 400, 400);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBackground(Color.lightGray);
		Label dataTicket = new Label(this.myFrame.date.getText(), Label.CENTER);
		Label timeTicket = new Label(this.myFrame.time.getText(), Label.CENTER);
		Label numerTicket = new Label(this.myFrame.number.getText(), Label.CENTER);
		Label nrRefTicket = new Label(this.myFrame.numberRef.getText() + " " +this.myFrame.branch.getText() ,Label.CENTER);
		nrRefTicket.setFont( new Font("Arial",56, 56));
		add(dataTicket);
		add(timeTicket);
		add(numerTicket);
		add(nrRefTicket);
		setVisible(true);
		addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosing(WindowEvent e) {
				setVisible(false);

			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

}
