
create sequence user_seq;
INSERT INTO user(id, login, password, full_name, role, score, bet_quantity, effectiveness) values (nextval('user_seq'), 'login', 'login', 'John Example', 'ROLE_USER', 3, 1, 100);
INSERT INTO user(id, login, password, full_name, role, score, bet_quantity, effectiveness) values (nextval('user_seq'), 'user1', 'user1', 'BetManiac', 'ROLE_USER', 6, 3, 67);
INSERT INTO user(id, login, password, full_name, role, score, bet_quantity, effectiveness) values (nextval('user_seq'), 'admin', 'admin', 'John Admin', 'ROLE_ADMIN', 5, 2, 50);

create sequence team_seq;
INSERT INTO team(id, name) values (nextval('team_seq'), 'Poland');
INSERT INTO team(id, name) values (nextval('team_seq'), 'Germany');
INSERT INTO team(id, name) values (nextval('team_seq'), 'Ukraine');
INSERT INTO team(id, name) values (nextval('team_seq'), 'North Ireland');
INSERT INTO team(id, name) values (nextval('team_seq'), 'Switzerland');
INSERT INTO team(id, name) values (nextval('team_seq'), 'Belgium');
INSERT INTO team(id, name) values (nextval('team_seq'), 'Italy');

create sequence match_seq;
INSERT INTO match(id, host_team_id, guest_team_id, host_score, guest_score, match_time) values (nextval('match_seq'), 1, 2, 0, 0, to_timestamp('2011-06-16 21:00', 'YYYY-MM-DD HH24:MI'));
INSERT INTO match(id, host_team_id, guest_team_id, host_score, guest_score, match_time) values (nextval('match_seq'), 1, 3, 1, 0, to_timestamp('2016-06-21 18:00', 'YYYY-MM-DD HH24:MI'));
INSERT INTO match(id, host_team_id, guest_team_id, host_score, guest_score, match_time) values (nextval('match_seq'), 1, 4, 1, 0, to_timestamp('2016-06-12 18:00', 'YYYY-MM-DD HH24:MI'));
INSERT INTO match(id, host_team_id, guest_team_id, host_score, guest_score, match_time) values (nextval('match_seq'), 1, 5, 1, 1, to_timestamp('2016-06-25 15:00', 'YYYY-MM-DD HH24:MI'));
INSERT INTO match(id, host_team_id, guest_team_id, host_score, guest_score, match_time) values (nextval('match_seq'), 1, 7, 4, 1, to_timestamp('2016-07-02 15:00', 'YYYY-MM-DD HH24:MI'));
INSERT INTO match(id, host_team_id, guest_team_id, host_score, guest_score, match_time) values (nextval('match_seq'), 1, 6, null, null, to_timestamp('2018-07-02 12:20', 'YYYY-MM-DD HH24:MI'));
INSERT INTO match(id, host_team_id, guest_team_id, host_score, guest_score, match_time) values (nextval('match_seq'), 1, 7, null, null, to_timestamp('2018-06-30 21:20', 'YYYY-MM-DD HH24:MI'));
INSERT INTO match(id, host_team_id, guest_team_id, host_score, guest_score, match_time) values (nextval('match_seq'), 2, 7, null, null, to_timestamp('2016-08-06 18:00', 'YYYY-MM-DD HH24:MI'));

create sequence bet_seq;
INSERT INTO bet(id, user_id, match_id, host_score, guest_score, points) values (nextval('bet_seq'), 1, 1, 2, 1, 3);
INSERT INTO bet(id, user_id, match_id, host_score, guest_score, points) values (nextval('bet_seq'), 2, 2, 1, 1, 1);
INSERT INTO bet(id, user_id, match_id, host_score, guest_score, points) values (nextval('bet_seq'), 3, 5, 4, 1, 5);
INSERT INTO bet(id, user_id, match_id, host_score, guest_score, points) values (nextval('bet_seq'), 2, 5, 2, 2, null);

commit;