<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 10px;
}
</style>
</head>
<body>
	<h2>Add new match</h2>
	<spring:url value="/admin/match?action=update" var="updateMatchAction" />	
	<form:form method="post" modelAttribute="match" action="${updateMatchAction}">
		<table>
			<tr>
				<th>Match time</th>
				<th>Host team</th>
				<th>Guest team</th>
				<th>Result</th>
			</tr>
			<tr>			
				<form:hidden path="id" />	
				<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${match.matchTime}" />
					<form:hidden path="matchTime" />
				</td>
				<td>${match.hostTeam.name}
					<form:hidden path="hostTeam.id" />
					<form:hidden path="hostTeam.name" />
				</td>
				<td>${match.guestTeam.name}
					<form:hidden path="guestTeam.id" />
					<form:hidden path="guestTeam.name" />
				</td>
				<td>
					<spring:bind path="hostScore">
						<form:input path="hostScore" size="2"/>
						<form:errors path="hostScore" />
					</spring:bind>
					:					
					<spring:bind path="guestScore">
						<form:input path="guestScore" size="2"/>
						<form:errors path="guestScore" />
					</spring:bind>				
				</td>
			</tr>
			<tr><td colspan="5"><button type="submit">Set result</button></td></tr>
		</table>
	</form:form>
	
	<spring:url value="/user/matches" var="backAction" />
	<h2><a href="${backAction}">Back</a></h2>

</body>
</html>