<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 10px;
}
</style>
</head>
<body>
	<h2>Adding new bet</h2>
	<spring:url value="/user/bets/add" var="addBetAction" />
	<form:form method="post" modelAttribute="bet" action="${addBetAction}">
		<table>
			<tr>
				<th>Match</th>
				<th>Predicted result</th>
			</tr>

			<c:choose>
				<c:when test="${avalaibleMatchesSize>0}">
					<tr>


						<form:hidden value="${userID}" path="owner.id" />

						<td><form:select path="match.id">
								<form:options items="${avalaibleMatches}" />
							</form:select></td>
						<td><spring:bind path="hostScore">
								<form:input path="hostScore" size="2" />
								<form:errors path="hostScore" />
							</spring:bind> : <spring:bind path="guestScore">
								<form:input path="guestScore" size="2" />
								<form:errors path="guestScore" />
							</spring:bind></td>

					</tr>
					<tr>
						<td colspan="3"><button type="submit">Add</button></td>
					</tr>
				</c:when>
				<c:otherwise>
							<tr><td colspan="2">No events avalaible.</td></tr>
				</c:otherwise>
			</c:choose>
		</table>
	</form:form>

	<spring:url value="/user/bets" var="backAction" />
	<h2>
		<a href="${backAction}">Back</a>
	</h2>

</body>
</html>