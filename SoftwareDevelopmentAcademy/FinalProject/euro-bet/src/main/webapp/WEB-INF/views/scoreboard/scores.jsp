<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 10px;
}
</style>
</head>
<body>
	<h2>Score Table: </h2>
	<table>
		<tr>
			<th>User</th>			
			<th>Points</th>
			<th>Bet's Quantity</th>
			<th>Effectiveness</th>			
		</tr>
		<c:forEach var="user" items="${rank}">						
			<tr>
				<td>${user.userName}</td>
				<td>${user.points}</td>
				<td>${user.betsQuantity}</td>	
				<td>${user.effectiveness} %</td>					
			</tr>
		</c:forEach>
	</table>
	
	<spring:url value="/" var="homeAction" />
	<h2><a href="${homeAction}">Home</a></h2>

</body>
</html>