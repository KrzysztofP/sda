<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 10px;
}
</style>
</head>
<body>
	<h2>Add new match</h2>
	<spring:url value="/admin/match?action=add" var="addMatchAction" />	
	<form:form method="post" modelAttribute="match" action="${addMatchAction}">
		<table>
			<tr>
				<th>Match time</th>
				<th>Host team</th>
				<th>Guest team</th>
			</tr>
			<tr>				
				<td>
					<spring:bind path="matchTime">				
						<form:input path="matchTime" type="datetime" id="matchTime" value="yyyy-MM-dd HH:mm" />
						<form:errors path="matchTime" />
					</spring:bind>
				</td>
				<td>
					<spring:bind path="hostTeam">
						<form:select path="hostTeam.id">
    						<form:options items="${teams}" />
						</form:select>
						<form:errors path="hostTeam" />
					</spring:bind>
				</td>
				<td>
					<spring:bind path="guestTeam">
						<form:select path="guestTeam.id">
    						<form:options items="${teams}" />
						</form:select>
						<form:errors path="guestTeam" />
					</spring:bind>
				</td>
			</tr>
			<tr><td colspan="3"><button type="submit">Add</button></td></tr>
		</table>
	</form:form>
	
	<spring:url value="/user/matches" var="backAction" />
	<h2><a href="${backAction}">Back</a></h2>

</body>
</html>