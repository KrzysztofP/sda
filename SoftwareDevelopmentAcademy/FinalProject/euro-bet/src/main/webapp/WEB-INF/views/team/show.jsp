<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<body>

	<spring:url value="/admin/team" var="addTeamAction" />	
	<h2>Teams</h2>
	<table>
		<c:forEach var="team" items="${teams}">
			<spring:url value="/admin/team/${team.id}/delete" var="deleteTeamAction" />
			<tr>
				<td>${team.name}</td>
				<c:if test="${!team.memberOfAnyMatch}">
					<td><a href="${deleteTeamAction}">Delete</a></td>
				</c:if>
			</tr>
		</c:forEach>
	</table>
	
	<h2>Add new team</h2>
	
	<form:form method="post" modelAttribute="team" action="${addTeamAction}">
		<table>
			<tr>
				<td><label>Team name</label></td>
				<td>
					<spring:bind path="name">
						<form:input path="name"  id="name" />
						<form:errors path="name" />
					</spring:bind>
				</td>
				<td>
					<button type="submit">Add</button>
				</td>
			</tr>
		</table>
	</form:form>
	
	<spring:url value="/" var="homeAction" />
	<h2><a href="${homeAction}">Home</a></h2>
</body>
</html>