<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 10px;
}
</style>
</head>
<body>
	<h2>Your finished bets: </h2>
	<table>
		<tr>
			<th>Match</th>			
			<th>Predicted result</th>
			<th>Actual result</th>
			<th>Points gained</th>
		</tr>
		<c:forEach var="bet" items="${historicalBets}">			
			<spring:url value="/user/bets/add" var="addBetAction" />	
			<tr>
				<td>${bet.match.hostTeam.name} : ${bet.match.guestTeam.name}</td>
				<td>${bet.hostScore} : ${bet.guestScore}</td>
				<td>
					<c:choose>
						<c:when test="${bet.match.hostScore != null && bet.match.guestScore != null}">
							${bet.match.hostScore} : ${bet.match.guestScore}	
						</c:when>
						<c:otherwise>
							Match haven't been played yet
						</c:otherwise>
					</c:choose>				
				</td>
				<td>
					${bet.points}							
				</td>
			</tr>
		</c:forEach>
	</table>
	
	<h2>Your pending bets: </h2>
	<table>
		<tr>
			<th>Match</th>			
			<th>Predicted result</th>
		</tr>
		<c:forEach var="bet" items="${pendingBets}">			
			
			<tr>
				<td>${bet.match.hostTeam.name} : ${bet.match.guestTeam.name}</td>
				<td>${bet.hostScore} : ${bet.guestScore}</td>
			</tr>
		</c:forEach>
	</table>
	
	<spring:url value="/user/bets/addview" var="addBetAction" />	
	<h2><a href="${addBetAction}">Add new bet</a></h2>
	
	<spring:url value="/" var="homeAction" />
	<h2><a href="${homeAction}">Home</a></h2>

</body>
</html>