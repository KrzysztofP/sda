<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 10px;
}
</style>
</head>
<body>
	
	<h2>Historical matches: </h2>
	<table>
		<tr>
			<th>Match time</th>
			<th>Hosts</th>
			<th>Guests</th>
			<th>Result</th>			
		</tr>
		<c:forEach var="match" items="${historicalMatches}">
			<spring:url value="/admin/match/${match.id}/update" var="updateMatchAction" />		
			<tr>
				<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${match.matchTime}" /></td>
				<td>${match.hostTeam.name}</td>
				<td>${match.guestTeam.name}</td>
				<td>
					<c:choose>
						<c:when test="${!match.resultEntered}">
							<sec:authorize access="hasRole('ROLE_ADMIN')">
								<a href="${updateMatchAction}">Enter result</a>
							</sec:authorize>	
							<sec:authorize access="!hasRole('ROLE_ADMIN')">
								Admin have not entered result yet
							</sec:authorize>	
						</c:when>
						<c:otherwise>
							${match.hostScore} : ${match.guestScore}
						</c:otherwise>
					</c:choose>
							
				</td>
			</tr>
		</c:forEach>
	</table>
	
	<spring:url value="/admin/match/add" var="addMatchAction" />	
	<h2><a href="${addMatchAction}">Add new match</a></h2>
	
	<h2>Not played matches: </h2>
	<table>
		<tr>
			<th>Match time</th>
			<th>Hosts</th>
			<th>Guests</th>
			<sec:authorize access="hasRole('ROLE_ADMIN')">
			<th>Actions</th>
			</sec:authorize>
		</tr>
		<c:forEach var="match" items="${notPlayedMatches}">			
			<spring:url value="/user/bet/add?matchId=${match.id}" var="addBetAction" />	
			<tr>
				<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${match.matchTime}" /></td>
				<td>${match.hostTeam.name}</td>
				<td>${match.guestTeam.name}</td>
				<sec:authorize access="hasRole('ROLE_ADMIN')">
				<spring:url value="/admin/matches/${match.id}/delete" var="deleteTeamAction" />

				<spring:url value="/admin/matches/${match.id}/delete" var="deleteTeamAction" />
				<td>
					
					<a href="${deleteTeamAction}">Delete match</a>				
				</td>
				</sec:authorize>
			</tr>
		</c:forEach>
	</table>
	
	<spring:url value="/" var="homeAction" />
	<h2><a href="${homeAction}">Home</a></h2>

</body>
</html>