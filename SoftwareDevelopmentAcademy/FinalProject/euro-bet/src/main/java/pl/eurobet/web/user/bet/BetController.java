package pl.eurobet.web.user.bet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pl.eurobet.core.bet.Bet;
import pl.eurobet.core.bet.BetDao;
import pl.eurobet.core.match.Match;
import pl.eurobet.core.match.MatchDao;
import pl.eurobet.core.user.UserProvider;

@Controller
public class BetController {

	@Autowired
	private BetDao betDao;

	@Autowired
	private MatchDao matchDao;

	@Autowired
	private UserProvider up;

	@RequestMapping("/user/bets")
	public ModelAndView showBetList() {
		ModelAndView modelAndView = new ModelAndView("bet/show");
		List<Bet> bets = betDao.getByUserId(up.getLoggedUser().getId());
		List<Bet> historicalBets = bets.stream().filter(m -> m.getPoints() != null).collect(Collectors.toList());
		List<Bet> pendingBets = bets.stream().filter(m -> m.getPoints() == null).collect(Collectors.toList());
		modelAndView.addObject("historicalBets", historicalBets);
		modelAndView.addObject("pendingBets", pendingBets);
		modelAndView.addObject("bet", new Bet());
		return modelAndView;
	}

	@RequestMapping(value = "/user/bets/addview", method = RequestMethod.GET)
	public ModelAndView showAvalaibleMatchesList() {
		ModelAndView modelAndView = new ModelAndView("bet/add");
		List<Match> matches = matchDao.getAll();
		Date now = new Date();
		List<Match> avalaibleMatches = matches.stream().filter(m -> m != null && m.getMatchTime().after(now))
				.collect(Collectors.toList());
		Collections.sort(avalaibleMatches, (o1, o2) -> o1.getMatchTime().compareTo(o2.getMatchTime()));
		modelAndView.addObject("avalaibleMatches", this.getMapByListMatch(avalaibleMatches));
		modelAndView.addObject("avalaibleMatchesSize", this.getMapByListMatch(avalaibleMatches).size());
		modelAndView.addObject("userID", up.getLoggedUser().getId());
		modelAndView.addObject("bet", new Bet());
		return modelAndView;
	}

	@RequestMapping(value = "/user/bets/add", method = RequestMethod.POST)
	public ModelAndView saveBet(@ModelAttribute @Validated Bet bet, BindingResult bindingResult) {
		betDao.saveOrUpdate(bet);
		return new ModelAndView("redirect:/user/bets");
	}

	private Map<Long, String> getMapByListMatch(List<Match> list) {
		Map<Long, String> map = new HashMap<>();
		List<Long> userMatchId = new ArrayList<>();
		List<Bet> userBets = betDao.getByUserId(up.getLoggedUser().getId());

		for (Bet bet : userBets) {
			userMatchId.add(bet.getMatch().getId());
		}

		for (Match match : list) {

			if (!userMatchId.contains(match.getId())) {
				map.put(match.getId(), match.toString());
			}
		}

		return map;
	}
}
