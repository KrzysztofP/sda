package pl.eurobet.core.user;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import pl.eurobet.core.bet.Bet;

@Entity
public class User {
	
	@Id
	@GeneratedValue(generator = "userSeq")
	@SequenceGenerator(name = "userSeq", sequenceName = "user_seq")
	private Long id;
	
	@Column
	private String login;
	
	@Column
	private String password;
	
	@Column (name = "full_name")
	private String fullName;
	
	@Column
	@Enumerated(EnumType.STRING)
	private UserRole role;

	@OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
	private Set<Bet> bets;
	
	@Column
	private Long score;
	
	@Column (name = "bet_quantity")
	private Long betQuantity;

	@Column
	private Long effectiveness;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public Set<Bet> getBets() {
		return bets;
	}
	
	public void setBets(Set<Bet> bets) {
		this.bets = bets;
	}
	
	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Long getBetQuantity() {
		return betQuantity;
	}

	public void setBetQuantity(Long betQuantity) {
		this.betQuantity = betQuantity;
	}

	public Long getEffectiveness() {
		return effectiveness;
	}

	public void setEffectiveness(Long effectiveness) {
		this.effectiveness = effectiveness;
	}

}
