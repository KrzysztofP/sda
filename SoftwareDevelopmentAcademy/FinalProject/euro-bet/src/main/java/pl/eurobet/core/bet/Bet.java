package pl.eurobet.core.bet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import pl.eurobet.core.match.Match;
import pl.eurobet.core.user.User;

@Entity
public class Bet {

	@Id
	@GeneratedValue(generator = "betSeq")
	@SequenceGenerator(name = "betSeq", sequenceName = "bet_seq")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User owner;

	@ManyToOne
	@JoinColumn(name = "match_id")
	private Match match;

	@Column
	private Long hostScore;

	@Column
	private Long guestScore;

	@Min(0)
	@Max(99)
	@Column
	private Long points;

	public void updatePoints() {
		if (match.isResultEntered()) {
			if (isExactHit()) {
				points = 5L;
			} else if (isResultHit()) {
				points = 3L;
			} else {
				points = 0L;
			}
		}
	}

	private boolean isResultHit() {
		return match.getGuestScore().compareTo(match.getHostScore()) == guestScore.compareTo(hostScore);
	}

	private boolean isExactHit() {
		return guestScore == match.getGuestScore() && hostScore == match.getHostScore();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Match getMatch() {
		return match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public Long getHostScore() {
		return hostScore;
	}

	public void setHostScore(Long hostScore) {
		this.hostScore = hostScore;
	}

	public Long getGuestScore() {
		return guestScore;
	}

	public void setGuestScore(Long guestScore) {
		this.guestScore = guestScore;
	}

	public Long getPoints() {
		return points;
	}

	public void setPoints(Long points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "Bet [id=" + id + ", owner=" + owner + ", match=" + match + ", hostScore=" + hostScore + ", guestScore="
				+ guestScore + ", points=" + points + "]";
	}

}
