package pl.eurobet.web.admin.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pl.eurobet.core.team.Team;
import pl.eurobet.core.team.TeamDao;

@Controller
public class TeamController {

	@Autowired
	private TeamDao teamDao;

	@RequestMapping("/admin/teams")
	public ModelAndView showTeamList() {
		ModelAndView modelAndView = new ModelAndView("team/show");
		modelAndView.addObject("teams", teamDao.getAll());
		modelAndView.addObject("team", new Team());
		return modelAndView;
	}

	@RequestMapping(value = "/admin/team", method = RequestMethod.POST)
	public ModelAndView addTeam(@Validated @ModelAttribute Team team, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("team/show");
			modelAndView.addObject("teams", teamDao.getAll());
			return modelAndView;
		}
		teamDao.saveOrUpdate(team);
		return new ModelAndView("redirect:/admin/teams");
	}

	@RequestMapping(value = "/admin/team/{teamId}/delete", method = RequestMethod.GET)
	public String deleteTeam(@PathVariable Long teamId) {
		teamDao.deleteById(teamId);
		return "redirect:/admin/teams";
	}

}
