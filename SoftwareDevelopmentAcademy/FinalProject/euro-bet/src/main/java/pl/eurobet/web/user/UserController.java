package pl.eurobet.web.user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pl.eurobet.core.bet.Bet;
import pl.eurobet.core.bet.BetDao;
import pl.eurobet.core.rankview.RankView;
import pl.eurobet.core.user.User;
import pl.eurobet.core.user.UserDao;

@Controller
public class UserController {

	@Autowired
	private UserDao userDao;

	@Autowired
	private BetDao betDao;

	@RequestMapping("/user/scores")
	public ModelAndView showUserRankList() {

		List<User> rankList = userDao.getAll();
		Collections.sort(rankList, (o1, o2) -> o2.getScore().compareTo(o1.getScore()));

		ModelAndView modelAndView = new ModelAndView("scoreboard/scores");
		modelAndView.addObject("users", rankList);
		modelAndView.addObject("user", new User());
		modelAndView.addObject("rank", getRankView());
		return modelAndView;
	}

	private List<RankView> getRankView() {

		List<RankView> listRankView = new ArrayList<>();
		List<User> users = betDao.getUserWithBets();

		for (User user : users) {
			RankView rView = new RankView();
			Long points = 0L;
			Long threePointsHits = 0L;
			Long fivePointsHits = 0L;
			List<Bet> bets = betDao.getByUserId(user.getId()).stream().filter(m -> m.getPoints() != null)
					.collect(Collectors.toList());
			for (Bet bet : bets) {
				points += bet.getPoints();
				
				if (bet.getPoints() == 3) {
					threePointsHits++;
					
				} else if(bet.getPoints() == 5){
					fivePointsHits++;
				}

			}

			rView.setUserName(user.getFullName());
			rView.setPoints(points);
			rView.setEffectiveness(100 * ((threePointsHits * 3) + (fivePointsHits * 5)) / points);
			rView.setBetsQuantity((long)bets.size());
			listRankView.add(rView);
		}

		return listRankView;

	}
	
	
}
