package pl.eurobet.core.match;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.eurobet.core.AbstractBaseDao;

@Repository
public class MatchDaoImpl extends AbstractBaseDao<Match, Long> implements MatchDao {

	@Override
	protected Class<Match> supports() {
		return Match.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Match> getNotPlayedMatchesOrderedByDate() {
		return currentSession().createCriteria(Match.class).add(Restrictions.gt("matchTime", new Date()))
				.addOrder(Order.desc("matchTime")).list();
	}

	@Override
	public void deleteById(Long matchId) {
		currentSession().createQuery("delete from Match where id = :matchId").setParameter("matchId", matchId)
				.executeUpdate();

	}

}
