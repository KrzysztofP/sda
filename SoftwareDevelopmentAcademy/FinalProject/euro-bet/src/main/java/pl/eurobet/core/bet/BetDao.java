package pl.eurobet.core.bet;

import java.util.List;

import pl.eurobet.core.BaseDao;
import pl.eurobet.core.user.User;

public interface BetDao extends BaseDao<Bet, Long> {

	List<Bet> getByUserId(Long id);

	public List<User> getUserWithBets();
}
