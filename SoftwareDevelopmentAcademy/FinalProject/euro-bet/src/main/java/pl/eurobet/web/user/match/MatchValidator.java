package pl.eurobet.web.user.match;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.eurobet.core.match.Match;

@Component
public class MatchValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return Match.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		Match match = (Match) target;

		ValidationUtils.rejectIfEmpty(errors, "matchTime", "validation.match.emptyField");
		ValidationUtils.rejectIfEmpty(errors, "hostTeam", "validation.match.emptyField");
		ValidationUtils.rejectIfEmpty(errors, "guestTeam", "validation.match.emptyField");

		if (hasSameTeamOnBothSide(match)) {
			errors.rejectValue("hostTeam", "validation.match.sameTeams");
		}

	}

	private boolean hasSameTeamOnBothSide(Match match) {
		return match.getHostTeam().getId().equals(match.getGuestTeam().getId());
	}

}
