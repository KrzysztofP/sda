package pl.eurobet.core.user;

import pl.eurobet.core.BaseDao;

public interface UserDao extends BaseDao<User, Long> {
	
	User getByLogin(String login);
	

}
