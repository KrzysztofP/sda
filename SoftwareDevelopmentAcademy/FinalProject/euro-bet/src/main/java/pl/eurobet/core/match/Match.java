package pl.eurobet.core.match;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;

import pl.eurobet.core.bet.Bet;
import pl.eurobet.core.team.Team;

@Entity
public class Match {

	@Id
	@GeneratedValue(generator = "matchSeq")
	@SequenceGenerator(name = "matchSeq", sequenceName = "match_seq")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "host_team_id")
	private Team hostTeam;

	@ManyToOne
	@JoinColumn(name = "guest_team_id")
	private Team guestTeam;

	@Min(0)
	@Max(99)
	@Column
	private Long hostScore;

	@Min(0)
	@Max(99)
	@Column
	private Long guestScore;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	@Column
	private Date matchTime;

	@OneToMany(mappedBy = "match")
	private List<Bet> bets;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Team getHostTeam() {
		return hostTeam;
	}

	public void setHostTeam(Team hostTeam) {
		this.hostTeam = hostTeam;
	}

	public Team getGuestTeam() {
		return guestTeam;
	}

	public void setGuestTeam(Team guestTeam) {
		this.guestTeam = guestTeam;
	}

	public Long getHostScore() {
		return hostScore;
	}

	public void setHostScore(Long hostScore) {
		this.hostScore = hostScore;
	}

	public Long getGuestScore() {
		return guestScore;
	}

	public void setGuestScore(Long guestScore) {
		this.guestScore = guestScore;
	}

	public Date getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(Date matchTime) {
		this.matchTime = matchTime;
	}

	public List<Bet> getBets() {
		return bets;
	}

	public boolean isResultEntered() {
		return guestScore != null && hostScore != null;
	}

	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return sdf.format(matchTime) + " " + hostTeam.getName() + " - " + guestTeam.getName();
	}

}
