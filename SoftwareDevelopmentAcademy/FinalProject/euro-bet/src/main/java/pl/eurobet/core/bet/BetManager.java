package pl.eurobet.core.bet;

import java.util.Map;
import java.util.Set;

import pl.eurobet.core.match.Match;
import pl.eurobet.core.user.User;

public interface BetManager {

	Set<Bet> getCurrentUserBets();

	void saveBet(Bet bet);

	Bet createBetTemplateForMatch(Long matchId);

	Bet createBetTemplate();

	void updatePointsForEndedMatch(Match endedMatch);

	Map<User, Long> getUserPointsMap();

}
