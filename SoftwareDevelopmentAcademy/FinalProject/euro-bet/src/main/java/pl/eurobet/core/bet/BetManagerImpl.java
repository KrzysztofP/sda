package pl.eurobet.core.bet;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import pl.eurobet.core.match.Match;
import pl.eurobet.core.match.MatchDao;
import pl.eurobet.core.user.User;
import pl.eurobet.core.user.UserDao;
import pl.eurobet.core.user.UserProvider;

@Component
@Transactional
public class BetManagerImpl implements BetManager {

	@Autowired
	private BetDao betDao;

	@Autowired
	private MatchDao matchDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserProvider userProvider;

	@Override
	public Set<Bet> getCurrentUserBets() {
		User loggedUser = userProvider.getLoggedUser();
		return loggedUser.getBets();
	}

	@Override
	public void saveBet(Bet bet) {
		betDao.saveOrUpdate(bet);
	}

	@Override
	public void updatePointsForEndedMatch(Match endedMatch) {
		endedMatch = matchDao.getById(endedMatch.getId());
		for (Bet bet : endedMatch.getBets()) {
			bet.updatePoints();
		}
	}

	@Override
	public Bet createBetTemplateForMatch(Long matchId) {
		Bet template = new Bet();
		template.setMatch(matchDao.getById(matchId));
		template.setOwner(userProvider.getLoggedUser());
		return template;
	}

	@Override
	public Bet createBetTemplate() {
		Bet template = new Bet();
		template.setOwner(userProvider.getLoggedUser());
		return template;
	}

	@Override
	public Map<User, Long> getUserPointsMap() {
		Map<User, Long> results = new HashMap<>();
		userDao.getAll().forEach(u -> results.put(u, getPointsOfUser(u)));
		return results;
	}

	private Long getPointsOfUser(User u) {
		return u.getBets().stream().mapToLong(b -> b.getPoints() != null ? b.getPoints() : 0L).sum();
	}

}
