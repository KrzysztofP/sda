package pl.eurobet.core.match;

import java.util.List;

import pl.eurobet.core.BaseDao;

public interface MatchDao extends BaseDao<Match, Long> {

	List<Match> getNotPlayedMatchesOrderedByDate();

	void deleteById(Long matchId);

}
