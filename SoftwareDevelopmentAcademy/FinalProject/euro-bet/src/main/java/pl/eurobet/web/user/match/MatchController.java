package pl.eurobet.web.user.match;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.eurobet.core.bet.BetManager;
import pl.eurobet.core.match.Match;
import pl.eurobet.core.match.MatchDao;
import pl.eurobet.core.team.Team;
import pl.eurobet.core.team.TeamDao;

@Controller
public class MatchController {

	@Autowired
	private MatchDao matchDao;

	@Autowired
	private TeamDao teamDao;

	@Autowired
	private BetManager betManager;

	@Autowired
	private MatchValidator matchValidator;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(matchValidator);
	}

	@RequestMapping("/user/matches")
	public ModelAndView showMatchesList() {
		ModelAndView modelAndView = new ModelAndView("match/show");
		List<Match> matches = matchDao.getAll();
		Date now = new Date();
		List<Match> historicalMatches = matches.stream().filter(m -> m != null && m.getMatchTime().before(now))
				.collect(Collectors.toList());
		Collections.sort(historicalMatches, (o1, o2) -> o1.getMatchTime().compareTo(o2.getMatchTime()));
		List<Match> notPlayedMatches = matches.stream().filter(m -> m != null && m.getMatchTime().after(now))
				.collect(Collectors.toList());
		Collections.sort(notPlayedMatches, (o1, o2) -> o1.getMatchTime().compareTo(o2.getMatchTime()));
		modelAndView.addObject("historicalMatches", historicalMatches);
		modelAndView.addObject("notPlayedMatches", notPlayedMatches);
		return modelAndView;
	}

	@RequestMapping("/admin/match/add")
	public ModelAndView getAddMatchForm() {
		ModelAndView modelAndView = new ModelAndView("match/add");
		modelAndView.addObject("match", new Match());
		populateModelWithTeams(modelAndView);
		return modelAndView;
	}

	@RequestMapping("/admin/match/{id}/update")
	public ModelAndView getUpdateMatchForm(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView("match/update");
		modelAndView.addObject("match", matchDao.getById(id));
		return modelAndView;
	}

	private void populateModelWithTeams(ModelAndView modelAndView) {
		List<Team> teamsList = teamDao.getAll();
		modelAndView.addObject("teams", buildMap(teamsList));
	}

	@RequestMapping(value = "/admin/match", method = RequestMethod.POST)
	public ModelAndView saveMatch(@ModelAttribute @Validated Match match, BindingResult bindingResult,
			@RequestParam String action) {
		if (bindingResult.hasErrors()) {
			return getViewForRetype(action);
		}
		match.setGuestTeam(teamDao.getById(match.getGuestTeam().getId()));
		match.setHostTeam(teamDao.getById(match.getHostTeam().getId()));
		matchDao.saveOrUpdate(match);
		if (action.equals("update")) {
			betManager.updatePointsForEndedMatch(match);
		}
		return new ModelAndView("redirect:/user/matches");
	}

	@RequestMapping(value = "/admin/matches/{matchId}/delete", method = RequestMethod.GET)
	public String deleteMatch(@PathVariable Long matchId) {
		matchDao.deleteById(matchId);
		return "redirect:/user/matches";
	}

	private ModelAndView getViewForRetype(String action) {
		ModelAndView modelAndView = new ModelAndView();
		if (action.equals("add")) {
			modelAndView.setViewName("match/add");
			populateModelWithTeams(modelAndView);
		} else {
			modelAndView.setViewName("match/update");
		}
		return modelAndView;
	}

	private Map<Long, String> buildMap(List<Team> teamsList) {
		Map<Long, String> teamMap = new HashMap<>();
		for (Team team : teamsList) {
			teamMap.put(team.getId(), team.getName());
		}
		return teamMap;
	}

}
