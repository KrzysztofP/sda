package pl.eurobet.core.team;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;

import pl.eurobet.core.match.Match;

@Entity
public class Team {

	@Id
	@GeneratedValue(generator = "teamSeq")
	@SequenceGenerator(name = "teamSeq", sequenceName = "team_seq")
	private Long id;

	@Size(min = 3, max = 50)
	@Column
	private String name;

	@OneToMany(mappedBy = "hostTeam", fetch = FetchType.EAGER)
	private List<Match> matchesAsHost;

	@OneToMany(mappedBy = "guestTeam", fetch = FetchType.EAGER)
	private List<Match> matchesAsGuest;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<Match> getMatchesAsHost() {
		return matchesAsHost;
	}

	public List<Match> getMatchesAsGuest() {
		return matchesAsGuest;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isMemberOfAnyMatch() {
		return !matchesAsGuest.isEmpty() || !matchesAsHost.isEmpty();
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Team [id=" + id + ", name=" + name + ", matchesAsHost=" + matchesAsHost + ", matchesAsGuest="
				+ matchesAsGuest + "]";
	}

}
