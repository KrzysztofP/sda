package pl.eurobet.core.user;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.eurobet.core.AbstractBaseDao;

@Repository
public class UserDaoImpl extends AbstractBaseDao<User, Long> implements UserDao {

	@Override
	protected Class<User> supports() {
		return User.class;
	}

	public User getByLogin(String login) {
		return (User) currentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("login", login))
				.uniqueResult();
	}



}
