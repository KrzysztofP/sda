package pl.eurobet.core.team;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.eurobet.core.AbstractBaseDao;

@Transactional
@Repository
public class TeamDaoImpl extends AbstractBaseDao<Team, Long> implements TeamDao {

	@Override
	protected Class<Team> supports() {
		return Team.class;
	}

	@Override
	public void deleteById(Long id) {
		currentSession().createQuery("delete from Team where id = :teamId")
		.setParameter("teamId", id)
		.executeUpdate();
	}

}
