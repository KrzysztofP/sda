package pl.eurobet.core.bet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import pl.eurobet.core.AbstractBaseDao;
import pl.eurobet.core.user.User;

@Repository
public class BetDaoImpl extends AbstractBaseDao<Bet, Long> implements BetDao {

	@Override
	protected Class<Bet> supports() {
		return Bet.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Bet> getByUserId(Long id) {
		return currentSession().createQuery("FROM Bet WHERE user_id = :betId").setParameter("betId", id).list();

	}

	@Override
	public List<User> getUserWithBets() {

		Set<User> users = new HashSet<>();
		List<Bet> listBets = this.getAll();
		for (Bet bet : listBets) {
			users.add(bet.getOwner());
		}

		return new ArrayList<>(users);
	}

}
