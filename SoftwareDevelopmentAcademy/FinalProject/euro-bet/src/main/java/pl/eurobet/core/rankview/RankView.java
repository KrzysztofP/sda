package pl.eurobet.core.rankview;

public class RankView {
	private String userName;

	private Long points;

	private Long betsQuantity;

	private Long effectiveness;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getPoints() {
		return points;
	}

	public void setPoints(Long points2) {
		this.points = points2;
	}

	public Long getBetsQuantity() {
		return betsQuantity;
	}

	public void setBetsQuantity(Long betsQuantity) {
		this.betsQuantity = betsQuantity;
	}

	public Long getEffectiveness() {
		return effectiveness;
	}

	public void setEffectiveness(Long effectiveness) {
		this.effectiveness = effectiveness;
	}

}
