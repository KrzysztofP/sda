import javafx.application.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;


public class JavaFXLabel extends Application{

	Label response;
	Label heading;
	Label answerA;
	Label answerB;
	Label answerC;
	Label answerD;
	
	public static void main(String[] args) {
		
//		Uruchamia aplikacje JavaFX, wywolujac metode launch()
		launch(args);
	}
	
//	Przesloniecie metody start()
	public void start(Stage myStage){
		
//		Okresla tytul obszaru roboczego
		myStage.setTitle("MyOwnNamedLabel");
		
//		Uzywa obiektu FlowPane jako korzenia. (,) - pionowe i poziome odstepy pomiedzy
//		umieszczonymi w nim kontrolkami
//		FlowPane rootNode = new FlowPane(Orientation.VERTICAL, 10, 10);
		GridPane rootNode = new GridPane();

//		Wyrownuje kontrolki do srodka
		rootNode.setAlignment(Pos.CENTER);

//		Tworzy scene w obszarze roboczym
		Scene myScene = new Scene(rootNode, 400, 200);
		
//		Ustawia scene w obszarze roboczym
		myStage.setScene(myScene);
	
//		Tworzy etykiete
		heading = new Label("The question is...");
	    GridPane.setConstraints(heading, 1, 0);
		
//		Tworzy przyciski i ustawia ich pozycje
//	    GridPane.setConstraints(buton_name, columns, row);
		Button btnA = new Button("A");
	    GridPane.setConstraints(btnA, 0, 1);
		Button btnB = new Button("B");
	    GridPane.setConstraints(btnB, 0, 2);
		Button btnC = new Button("C");
	    GridPane.setConstraints(btnC, 0, 3);
		Button btnD = new Button("D");
	    GridPane.setConstraints(btnD, 0, 4);
	    Button btnNO = new Button("NO");
	    GridPane.setConstraints(btnNO, 0, 6);
	    Button btnYES = new Button("YES");
	    GridPane.setConstraints(btnYES, 2, 6);
		
		answerA = new Label("aaa");
	    GridPane.setConstraints(answerA, 1, 1);
		answerB = new Label("bbb");
	    GridPane.setConstraints(answerB, 1, 2);
		answerC = new Label("ccc");
	    GridPane.setConstraints(answerC, 1, 3);
		answerD = new Label("ddd");
	    GridPane.setConstraints(answerD, 1, 4);
	    
	    
//		Obsluga zdarzenia ActionEvent przyciskow A B C D
		btnA.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent ae){
				response.setText("You have choosen 'A'. Are you sure?");
			}		
		});
		btnB.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent ae){
				response.setText("You have choosen 'B'. Are you sure?");
			}		
		});
		btnC.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent ae){
				response.setText("You have choosen 'C'. Are you sure?");
			}		
		});
		btnD.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent ae){
				response.setText("You have choosen 'D'. Are you sure?");
			}		
		});
		/* akcja po wcisnieciu YES - sprawdzenie odp. */
//		btnYES.setOnAction(new EventHandler<ActionEvent>(){
//			public void handle(ActionEvent ae){
//
//			}		
//		});
		response = new Label("And your answer is...");
	    GridPane.setConstraints(response, 1, 5);
//		Dodaje etykiete do grafu sceny
		rootNode.getChildren().addAll(heading, btnA, answerA, btnB, answerB, btnC, answerC, btnD, answerD, response, btnNO, btnYES);

		
//		Wyswietlanie obszaru roboczego i sceny
		myStage.show();
	}
}
